Program je pisan u pythonu 3.9

Prije pokretanja potrebno je u komandnoj liniji izvršiti komandu
pip install pycryptodome

Ovaj program služi kao password manager koji sigurno čuva više zaporki koristeći
AES-CBC, HMAC te scrypt.
Inicijalizacija se ostvaruje komandom
"python3 srs_lab1_MT.py init <master_password>"

Stavljanje novih parova "username" i "password" ostvaruje se komandom
"python3 srs_lab1_MT.py put <master_password> <username> <password>"

Dohvaćanje lozinke <password> za određeni <username> ostvaruje se komandom
"python3 srs_lab1_MT.py get <master_password> <username>"

