from queue import Empty
import sys
import json
from base64 import b64encode
from base64 import b64decode
from warnings import catch_warnings
from Crypto.Util.Padding import pad
from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Protocol.KDF import scrypt
from Crypto.Hash import HMAC, SHA256


dictionary = {}
salt = None
iv = None
hmacc = None
AESc = None
hash_fail = 0

def init():
    file = open("db.txt", "w+")
    file = open("h.txt", "w+")
    file = open("salt.txt", "w+")
    file.write(b64encode(get_random_bytes(16)).decode('utf-8'))
    file.close()
    print("Password manager initialized")

def put(name, passw):
    global dictionary
    try:
        file = open("db.txt", "r+")
        fileRead = file.read()
        file.close()
        hfile = open("h.txt","r")
        hRead = hfile.read()
        if(fileRead):
            dehash(str.encode(fileRead))           
            dictionary = json.loads(decrypt(fileRead, hRead))
            dictionary[name] = passw
            json_string = json.dumps(dictionary).encode('utf-8')
            save = encrypt(json_string)
            save = AESc

            file = open("db.txt", "w+")
            file.write(save)
            file.close()
            print("Saved password for:",name)
        else:
            dictionary[name] = passw
            json_string = json.dumps(dictionary).encode('utf-8')
            save = encrypt(json_string)
            save = AESc
            file = open("db.txt", "w+")
            file.write(save)
            print("Saved password for:",name)
            file.close()
    except:
        print("tugy")
    

    
    


def get(name):
    try:
        global dictionary
        file = open("db.txt", "r+")
        fileRead = file.read()
        file.close()
        hfile = open("h.txt","r")
        hRead = hfile.read()
        if(fileRead):
            dehash(str.encode(fileRead))
                
            dictionary = json.loads(decrypt(fileRead, hRead))
            if(hash_fail == 0):
                print("Password for",name, "is: ", dictionary[name])
            return (dictionary[name]) 

    except:
        print("FAILED TO GET PASS")

def encrypt(data):
    global salt
    global AESc
    sFile = open("salt.txt", "r+")
    salt = sFile.read()
    sFile.close()
    #key1 za AES, key2 za hmac
    key1, key2= scrypt(sys.argv[2], salt, 16, N=2**14, r=8, p=1, num_keys=2)

    cipher = AES.new(key1, AES.MODE_CBC)
    ct_bytes = cipher.encrypt(pad(data, AES.block_size))
    iv = b64encode(cipher.iv).decode('utf-8')
    ct = b64encode(ct_bytes).decode('utf-8')
    result = json.dumps({'iv':iv, 'ciphertext':ct})

    hash(data, str.encode(result))
    AESc = result
    return ct_bytes

def hash(data, ct_bytes):
    global salt
    sFile = open("salt.txt", "r+")
    salt = sFile.read()
    sFile.close()
    key1, key2= scrypt(sys.argv[2], salt, 16, N=2**14, r=8, p=1, num_keys=2)
    hmacc = HMAC.new(key2, digestmod=SHA256)
    hmacc.update(ct_bytes)
    hmacc_saved = hmacc.hexdigest()
    file = open("h.txt", "w+")
    file.write(hmacc_saved)
    file.close()
    


def decrypt(data, hmac):
    global salt
    sFile = open("salt.txt", "r+")
    salt = sFile.read()
    sFile.close()
    nk, nkk= scrypt(sys.argv[2], salt, 16, N=2**14, r=8, p=1, num_keys=2)
    try:
        b64 = json.loads(data)  
        iv = b64decode(b64['iv'])
        ct = b64decode(b64['ciphertext'])
        cipher = AES.new(nk, AES.MODE_CBC, iv)
        pt = unpad(cipher.decrypt(ct), AES.block_size)
        return(pt)
    except (ValueError, KeyError):
        print("Incorrect decryption")

def dehash(data):
    global salt
    global hash_fail
    sFile = open("salt.txt", "r+")
    salt = sFile.read()
    sFile.close()
    nk, nkk= scrypt(sys.argv[2], salt, 16, N=2**14, r=8, p=1, num_keys=2)
    hmaccc = HMAC.new(nkk, digestmod=SHA256)
    hmaccc.update(data)
    file = open("h.txt", "r+")
    fileRead= file.read()
    if(fileRead):
        hmacc_saved = fileRead
    try:
        hmaccc.hexverify(hmacc_saved)
    except ValueError:
        print("HMAC FAILED")
        hash_fail = 1
        

def main():
    global dictionary
    global salt
    global iv

##################################################################################################

    
    #poruka = encrypt(b"Matija voli Ivanu")

#^^^^^^^^^^^^^^enkripcija^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    #dehash(poruka)
    #decrypt(AESc, hmacc_saved)
 ##################################################################################################

    if (sys.argv[1] == "init"):
        init()

    elif (sys.argv[1] =="get"):
        get(sys.argv[3])

    elif (sys.argv[1] == "put"):
        put(sys.argv[3], sys.argv[4])
        



if __name__ == "__main__":
    main()
